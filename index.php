<?php require_once "./code.php"?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Academic Break</title>
</head>
<body>
	<h1>1st Act: Declaring Variables</h1>
		<p><?php echo "Your first name is: $firstName"; ?></p>
		<p><?php echo "Your last name is: $lastName"; ?></p>
		<p><?php echo "Your age is: $age" ?></p>
		<p><?php echo "Your address is: $address" ?></p>

	<h1>2nd Act: Convert Peso to Dollar</h1>	
		<p><?php echo "Your peso is converted to: $", convertToDollar($conversionRate = 0.018, $pesoValue = 34000) ?></p>

	<h1>3rd Act: Selection Control Structure</h1>
		<p><?php echo "Your department is: ", displayDepartment(1) ?></p>
		<p><?php echo "Your department is: ", displayDepartment(5) ?></p>
		<p><?php echo "Your department is: ", displayDepartment(8) ?></p>
		<p><?php echo displayDepartment('A') ?></p>

</body>
</html>