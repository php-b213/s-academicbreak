<?php
	/* INSTRUCTIONS:
		- Create 4 variables that will contain values for first name, last name, age and address.
		- Access the declared variables to index.php and echo the values using paragraph tags.
		- Serve your php file to your local server using the terminal.
	*/

		$firstName = 'Clarisse Casi';
		$lastName = 'Carino';
		$age = 21;
		$address = 'Cavite, Philippines';

		//Function
			function convertToDollar($conversionRate, $pesoValue){
				$conversion = $pesoValue * $conversionRate;
				return $conversion;
			}

		//IF-ELSE Statement (Selection Control Structure)
			function displayDepartment($deptNumber){
				if($deptNumber >= 1 && $deptNumber <= 3){
					return "Department A";
				} 
				else if ($deptNumber >=4 && $deptNumber <=6) {
					return "Department B";
				}
				else if ($deptNumber >=7 && $deptNumber <= 9){
					return "Department C";
				} 
				else {
					return "Sorry, Department does not exist!";
				}
			}
?>